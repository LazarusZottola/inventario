from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.contrib import messages

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required, permission_required


from .models import Ingrediente, Producto
from .forms import IngredienteForm, ProductoForm
#from .forms import Categreverse.lazy


class IngredienteView(LoginRequiredMixin, generic.ListView):
    model = Ingrediente
    template_name = "inv/ingrediente_list.html"
    context_object_name = "obj"
    login_url = 'bases:login'
    #permission_required="inv.view_Ingrediente"

class IngredienteNew(LoginRequiredMixin, generic.CreateView):
    #permission_required="inv.add_ingrediente" 
    model=Ingrediente
    template_name="inv/ingrediente_form.html"
    context_object_name = "obj"
    form_class=IngredienteForm
    success_url=reverse_lazy("inv:ingrediente_list")
    login_url='bases:login'
 

    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


class IngredienteEdit(LoginRequiredMixin, generic.UpdateView):
    #permission_required="inv.add_ingrediente" 
    model=Ingrediente
    template_name="inv/ingrediente_form.html"
    context_object_name = "obj"
    form_class=IngredienteForm
    success_url=reverse_lazy("inv:ingrediente_list")
    login_url='bases:login'
 

    def form_valid(self, form):
        form.instance.um = self.request.user.id
        return super().form_valid(form)

class IngredienteDel(LoginRequiredMixin,generic.DeleteView):
    model=Ingrediente
    template_name='inv/ingredientes_del.html'
    context_object_name='obj'
    success_url=reverse_lazy("inv:ingrediente_list")

class ProductoView(LoginRequiredMixin, generic.ListView):
    model = Producto
    template_name = "inv/producto_list.html"
    context_object_name = "obj"
    login_url = 'bases:login'
    #permission_required="inv.view_Ingrediente"

class ProductoNew(LoginRequiredMixin, generic.CreateView):
    #permission_required="inv.add_ingrediente" 
    model=Producto
    template_name="inv/producto_form.html"
    context_object_name = "obj"
    form_class=ProductoForm
    success_url=reverse_lazy("inv:producto_list")
    login_url='bases:login'
 

    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


class ProductoEdit(LoginRequiredMixin, generic.UpdateView):
    #permission_required="inv.add_ingrediente" 
    model=Producto
    template_name="inv/producto_form.html"
    context_object_name = "obj"
    form_class=ProductoForm
    success_url=reverse_lazy("inv:producto_list")
    login_url='bases:login'
 

    def form_valid(self, form):
        form.instance.um = self.request.user.id
        return super().form_valid(form)

class ProductoDel(LoginRequiredMixin,generic.DeleteView):
    model=Producto
    template_name='inv/productos_del.html'
    context_object_name='obj'
    success_url=reverse_lazy("inv:producto_list")


