from django.urls import path

from .views import IngredienteView, IngredienteNew, IngredienteEdit, IngredienteDel, ProductoView, ProductoNew, ProductoEdit, ProductoDel

urlpatterns = [
    path('ingredientes/',IngredienteView.as_view(), name='ingrediente_list'),
    path('ingredientes/new',IngredienteNew.as_view(), name='ingrediente_new'),
    path('ingredientes/edit/<int:pk>',IngredienteEdit.as_view(), name='ingrediente_edit'),
    path('ingredientes/delate/<int:pk>',IngredienteDel.as_view(), name='ingrediente_del'),
    path('productos/',ProductoView.as_view(), name='producto_list'),
    path('productos/new',ProductoNew.as_view(), name='producto_new'),
    path('productos/edit/<int:pk>',ProductoEdit.as_view(), name='producto_edit'),
    path('productos/delate/<int:pk>',ProductoDel.as_view(), name='producto_del'),
]