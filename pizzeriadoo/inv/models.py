from django.db import models

from bases.models import ClaseModelo

class Ingrediente(ClaseModelo):
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción del Ingrediente',
        unique=True
    )

    def __str__(self):
        return '{}'.format(self.descripcion)

    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Ingrediente, self).save()

    class Meta:
        verbose_name_plural= "Ingredientes"

class Producto(ClaseModelo):
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción del Producto',
        unique=True
    )

    def __str__(self):
        return '{}'.format(self.descripcion)

    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Producto, self).save()

    class Meta:
        verbose_name_plural= "Productos"