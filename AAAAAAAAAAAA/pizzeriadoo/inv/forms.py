from django import forms

from .models import Ingrediente, Producto


class IngredienteForm(forms.ModelForm):
    class Meta:
        model=Ingrediente
        fields = ['descripcion','estado']
        labels = {'descripcion':"Descripción del Ingrediente",
               "estado":"Estado"}
        widget={'descripcion': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })


class ProductoForm(forms.ModelForm):
    class Meta:
        model=Producto
        fields = ['descripcion','estado']
        labels = {'descripcion':"Descripción del Producto",
               "estado":"Estado"}
        widget={'descripcion': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })